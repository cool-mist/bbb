---
layout: post
title:  Unfair fixtures!
categories: ["ideas", "cricket"]
---

Analyzing the fixtures of recently concluded IPL 2023. Was it set up fairly?

------

[IPL](https://en.wikipedia.org/wiki/2023_Indian_Premier_League) is the biggest cricket event in the world. The format of the game is generally
a double round robin followed by a double eliminator with the top 4 teams to decide the ultimate winner - modelled after the well recognized
"Group Stage followed by Playoffs" format.

**Group Stage**

Group Stage is used to seed the teams. A round robin format is when each team plays against every other team. This is pretty good. If
there are less number of teams, the fixture could include a double round robin instead, where each team plays against every other team
twice.

Let us assume that there are a total of 8 teams in the tournament.

Then each team has 7 opponents. Each team has to play each of those 7 teams twice, which means each team would play 14 games.
In total, the total number of games in the double round robin fixtures is going to be the number of teams in the tournament times
the number of matches played by each team, which is 14 times 8 = 112.

But not quite, you see out of the 14 games played by team 1, 2 of them are against team 2. We should not make the mistake of counting
that game again while calculating the total games played by team 2. That is - team 1 vs team 2 is the same as team 2 vs team 1. In the
above calculation, we have effectively counted each game twice - once as team 1 vs team 2 and once as team 2 vs team 1. Therefore the
total number of games is actually half of the above figure which is **56**.

At this point, as each team has already played twice against another, I would consider it fair to simply call the top team at the end
of this as the winner of the tournament. 

But people like watching close games - regardless of the skill level of the players. Under such a premise, the seeding done above could be put
to use to set up as many close games as possible.

**The Playoffs**

One way to set up the playoff matches is to pick the top teams of the tournament (preferably the top 4) and make them play against each other.
To make it fairer to the top performing teams, we could pit the top team against the bottom team and pit the second team against the
second-last team. It is likely that the first two teams are going to go to the finals based on what we might have already seen in the 
double round robin group stage. But hey... there could be "upsets".

A better alternative for playoffs follows..

Consider this setup for the top 4 seeded teams.

- Game 1: Team 1 plays against Team 2.
- Game 2: Team 3 plays against Team 4.
- Game 3: Winner of Game 1 plays against Winner of Game 2.
- Game 4: Loser of Game 1 plays against Loser of Game 2. Loser of this game is eliminated.
- Game 5: Winner of Game 4 plays against Loser of Game 3.
- Game 6: Winner of Game 5 plays against Winner of Game 3. 

Winner of Game 6 is the winner of the tournament. The final is between a team that has won all its games, against a team
that has lost exactly one game. If a team loses 2 games, then it is eliminated. This format also ensures that a team doesn't
go out on a single freak loss after playing the entire group stage properly. This format - is called a **double-eliminator**
since each team needs to lose twice to leave the tournament.

But most importantly, we now have mitigated the possibility of upsets!

> Note that to run a double-eliminator, one needs a number of teams that can be expressed as an exponent of 2 - 4, 8, 16 and so on.
Each game generates a winner and a loser who plays subsequently, or generates a winner and eliminates the other team. At the end of
the round where only a winner is generated, we would need a corresponding winner from another game to pair up in the next round.
This is only possible if halving the total number of teams after every such stage still leaves a number of teams that is divisible by 2.

**What does IPL do?**

IPL generally runs a double round robin for the Group stage and then a funky partial double eliminator for the Playoffs as follows.

- Game 1: Team 1 vs Team 2.
- Game 2: Team 3 vs Team 4. Loser is eliminated.
- Game 3: Winner of Game 2 vs Loser of Game 1.
- Game 4: Winner of Game 3 vs Winner of Game 1.

The top 2 teams get 2 chances. The bottom 2 teams are eliminated on their first loss. Pretty sure that the organizers would simply
explain this unfairness away by saying that the top teams are better than the bottom two and deserve more chances than the bottom
teams.

Traditionally there are 8 teams. That means 56 group stage matches and 4 playoff games resulting in a total of 60 games. This format
is pretty stupid spending more than 90 percent of the games on seeding only to finish off the tournament in a matter of 4 games.

IPL 2023 had 10 teams. If the organizers did what they normally do, the tournament would have ended up even more crazy - 

10 teams double round robin = 90 games in the group stage.
4 teams "funky" partial double eliminator = 4 games in the playoffs.

A total of 94 games with 90 games in the group stage!!

The only reason the organizers do this is because more matches - especially those that are guaranteed to take place without eliminations
(basically all group stage games) are money making machines - stadium tickets, and advertisements. The fairness of the fixtures isn't even
the 10th most important thing in this context.

Surprisingly though, IPL 2023 did something less crazy with 10 teams, the group stage had 70 games. And the playoffs 4.

70!??

How are there 70 games? If each team plays against the other team once, there should be 45 games. If they play twice, there should be 90.
How did each team play a fractional number of games against the other?

**More Teams = More Unfairness**

To figure this out, I started investigating the fixtures more deeply. Unfortunately, the organizing committee simply has a PDF file containing
the list of fixtures. That is not an easy format for anyone interesting in doing data analysis. Therefore, I took a detour trying to
parse and extract out this data in a more machine readable form. That took some time, but it was worth it. (The code is available
[here](https://github.com/cool-mist/ipl-data)).

Once I had the data in the right format, I could see that each team played exactly 14 games each. Clearly implying that they played atleast some
of them more than once as there are only 9 other teams.

I picked a team at random - Mumbai Indians, and looked at the games they have played.

They have played 2 games against the following teams and 1 game with the remaining teams. This means 5 times 2 plus 4 = 14.

- Gujarat Titans
- Royal Challengers Bangalore
- Punjab Kings
- Sunrisers Hyderabad
- Chennai Super Kings

I picked up another team - Delhi Capitals, and they played the same type of games, exactly 2 games against the same 5 teams above.

In fact, by bruteforcing my way through the entire team list, we can see that there have been 2 hidden groups. Group 1 is the group listed
above. The other Group 2 is

- Rajasthan Royals
- Lucknow Super Giants
- Delhi Capitals
- Kolkata Knight Riders
- Mumbai Indians

Each team played a single game against members of the same group, and 2 games against the opposite group. 

Somehow, they have made this even less fair than how it already is. To ensure that one side does not get to win too many games in the group stage,
the two virtual groups above have to be balanced. This balancing is probably done using the results of the previous year, but that doesn't mean
anything in this volatile tournament where players switch sides every year.

**A Fairer Alternative**

I propose below, a fairer alternative to the above nonsense, for 10 teams.

- A single round robin group stage for a total of 45 games.
- Eliminate the bottom 2 teams. A proper double elimination with the top 8 teams for a total of 14 games.

This brings the total number of games to 59! 1 Less than what IPL has been doing with 8 teams previously.

If required, we can throw in another game to identify the 3rd place in the playoffs as well to round it up to 60.

(The double eliminator for 8 teams with an optional 3rd place playoff is shown below).

![Double Eliminator]({{site.base_url | absolute_url}}/images/doubleeliminator.png)

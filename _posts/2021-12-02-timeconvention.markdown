---
layout: post
title: An Alternate Temporal Convention
categories: ["ideas"]
---

My proposal of an alternate convention for how we split and view the 24 hours in a day.

--------

## The Problem

A day is composed of 24 hours. In most places, the 24 hours on weekdays are roughly divided as follows. Times are in [24-hour clock](https://en.wikipedia.org/wiki/24-hour_clock) format.

- Around 6 hours of sleep from *`0000`* to *`0600`* when the sun hasn't risen yet.
- Around 12 hours of day time from *`0600`* to *`1800`*. Adults generally go to work and kids to school. The sun is up.
- Around 3 hours of family time from *`1800`* to *`2100`*. The sun has set.
- Around 3 hours of sleep from *`2100`* to *`2400`*/*`0000`*. It is dark outside.

> I see a couple of problems with this convention.

1. We have approximately 12 hours of day time and 12 hours of night time, but it is split unevenly across the 24-hour clock as
    - *Day* - *`0600`* to *`1800`*
    - *Night* - *`0000`* to *`0600`* and *`1800`* to *`2400`*

1. By the time people wake up, we are already approximately 6 hours in. It is absolutely ridiculous to start something from the number `6` instead of the number `0` or `1`.

## The Perfect split

To correct the above 2 problems, we can calibrate the time people wake up, and the time the sun rises, to be *`0000`* instead of *`0600`*.

- People wake up at around *`0000`*. Sun rises at around this time.
- Adults get ready to work by *`0300`*, Kids get to school by *`0200`*.
- Work time ends by *`1200`*. Kids get back home by around *`0900`* and play till *`1200`*.
- Family time from *`1200`*to *`1500`*.
- Sleep at the end of the day from *`1500`* to *`2400`*.

> People now wake up at the start of the day. The sun occupies the sky from *`0000`* to *`1200`*, while the moon stays from *`1200`* to *`2400`*.

The suffices **AM** and **PM** can be replaced by **D** for day and **N** for night. The 12-hour time notation would be simply **0 D** to **12 D** and **0 N** to **12 N**.

## Implications

People would require some time to adjust to this convention. However, I think this could be beneficial to humanity in the long run and worth the discomfort.

- All historical events will have to be shifted by one day on either side depending on the time of the day happened. This would be a one-time inconvenience at the time of implementing this change. This includes your birthday too!

- Holidays and festivals, now only start when people wake up. People don't have to stay up late to celebrate the new year or Christmas or a friend's birthday. They can come back from work the previous day and sleep. They need not leave work early for planning etc. Celebrations can start the next day after rejuvenation.

- Late-night parties can be converted into early-morning parties. It could be safer and healthier for people to party in broad daylight. Humans are not nocturnal creatures.

- When an alien civilization visits us, they would be impressed by our love for symmetry in our time keeping!!

## I'm sold, can we switch?

Not so fast. In all seriousness, the cost of adjusting to the new convention is too high that I do not see how this new system will ever be in use in our times.

However, **if** (or is it "when"??) human beings go extinct, I hope the next intelligent species that gets to shape the Earth, follows this convention while keeping time!!

---
layout: post
title: How to communicate securely? - 2
categories: ["ideas", "cryptography"]
---

How do we do that now? - For real! this time.

--------

This is part 2 of a series on communicating securely.
- [Part-1]({{site.base_url | absolute_url}}/securecommunication)
- [Part-2]({{site.base_url | absolute_url}}/reallysecure)
- [Part-3]({{site.base_url | absolute_url}}/finallysecure)

-------

Modern cryptography is indeed secure. However, malicious parties have anyway appeared on the scene under the guise of *Service Providers*.

# Who are these evil Service Providers?

Most people today are familiar with atleast 2 major providers - Google and Facebook. One of the most used communication platform (or service) is Google's emailing service - also called *Gmail*. Facebook has its own chat application and has now recently also acquired *Whatsapp*. For convenience, I will stick to these two applications for the rest of this post. The same arguments can be extended to most other providers.

These applications, are marketed as useful to the general public, helping them communicate with others over internet.

**How do they work?**

1. Companies distribute applications (called client applications) that send data to the company-owned server
1. The server stores the message.
1. If the recipient is online, the server delivers the message to the client application of the recipient.

The primary purpose of cryptography was to ensure that messages intercepted by a malicious party will still not be compromised. The asymmetric key cryptographic techniques lie at the pinnacle of all discoveries in this field.

When we look closely at the model of these companies, there is no notion of secrecy. And no effort is made to even hide the middle-man. The company server performs computation on these stored messages. The result of this computation will then be used to deliver targeted ads. Data collection is precisely the reason why these 'services' are provided free of cost by companies that make billions of dollars in profit.

Some companies - such as Whatsapp - have promised end-to-end encryption of messages. But, it would be foolish to take their word for it. The bottom line is that your messages are not safe, and they can and will be handed over to federal agencies when asked by them.

# I'm not doing anything illegal, why should I be scared?

This is the most common argument against being afraid of companies and agencies (and even the government) collecting data. The argument of agencies snooping in on our messages is that of security at a certain scope. Commercial companies can make a case for protecting you from malicious agents online. They can probably also defend such claims. Governmental agencies might make a similar argument with a defense terminating at national security. Civilians are expected to buy into these arguments (and some do) - resulting in a question along the lines of the title of this section.

## Problem of personal security

Is it not more secure for anyone, to not have their messages read and scanned by another party with unknown motivations? Clearly, a third party not reading into your messages is only going to make your messages more secure. When the data encryption techniques are known to all, and when it is straightforward to encrypt your own messages that precludes a need to trust a third party, it is foolish to continue using the services of third parties for communication.

Pressed further, one might say - the service is convenient to use, and they also take care of protecting us from malicious agents (eg: cyber criminals) online. This is a very strong point, but a point in whose favor? The reason malicious agents on these platforms exist is that there is a wide audience who have opted to use these services. These platforms are convenient for criminals too!!

Granted, convenience is to be aspired for, for something being done every day. But convenience cannot be a veil for a system void of security measures underneath. Manually encrypting and decrypting messages may not be as convenient as opening an application on your mobile phone and typing away messages, but it is more secure. (It is a trivial problem to create a mobile application for encrypted communication that bypasses a third party. It may just not be as convenient to use as what we are used to today).

## Problem of national security

This is a tough problem. On one hand, the Government wants to keep tabs on, or retrieve personal messages, for any suspected criminal. On the other hand, there is an ideological argument on intrusion into privacy and preventing a *[Big Brother](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four)* situation.

If we think about the need for intercepting and reading private conversations, it is likely that the antisocial elements already use encrypted communication bypassing any known platforms. They know that it is too risky and their communication is bound to be intercepted otherwise. The reason this is likely is that, again, the process of encryption and decryption is already known to everyone. This line of thinking looks intuitive and can be substantiated by data on how many crimes actually required law enforcement agencies to read private conversations of the criminals.

Given that the problem of national security is getting irrelevant in the context of secure communication, there is no reason for anyone to reveal their communication to any third parties.

# How do I protect myself?

The safest method I would recommend would be to use [PGP tools](https://en.wikipedia.org/wiki/Pretty_Good_Privacy).
- Create a public-private key-pair.
- Share the public key with anyone who wishes to communicate with you. Keep the private key a secret.
- Decrypt received messages (that were originally encrypted with your public key), using your private key.

The next post will cover specific details on encrypting and decrypting messages using the afore mentioned steps. For now, the following is my public key in case you wish to communicate with me through email at **neophytenuggets@proton.me**

(Unencrypted messages will be silently deleted :P)

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEY8PVmhYJKwYBBAHaRw8BAQdANxcKoo0fqmW62F2tQ7F0GPWkpGs/HQUyhR77
R2mgsY20JVN1cnlhIDxuZW9waHl0ZW51Z2dldHNAcHJvdG9ubWFpbC5tZT6ImQQT
FgoAQRYhBGixkTCdlU1PIjiQZSHehx95aKkIBQJjw9WaAhsDBQkDw4BOBQsJCAcC
AiICBhUKCQgLAgQWAgMBAh4HAheAAAoJECHehx95aKkIZB8A+wfMy7gqISQGXrIc
FiiwCzfoqzQfnJozLdaOA4MvU8RcAP9gY6U1Bm8LQDijr3LCFm7raLjBNBNwrWQg
cWuuqbiJDrg4BGPD1ZoSCisGAQQBl1UBBQEBB0AkrwBdf8M3EdsykIcYMBejeSHs
iiQGTLhZrYuvXtoNSgMBCAeIfgQYFgoAJhYhBGixkTCdlU1PIjiQZSHehx95aKkI
BQJjw9WaAhsMBQkDw4BOAAoJECHehx95aKkIF6sA/01X0YnHTjUo7nUaBzk4ub+3
Ub3FKJTeSooaQ39Y3V3QAQDH407drRMw3OgnRiSKDXsbjGhAec7vQip768acjYbq
Aw==
=Ioiu
-----END PGP PUBLIC KEY BLOCK-----
```


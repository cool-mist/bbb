---
layout: post
title: Will there be 128-bit computers?
categories: ["ideas"]
---

You are probably reading this on a 64-bit computer or a smartphone (a smartphone is technically a computer). What does 64 mean here? Were there other types? Will there be more?

--------

# The language of processors

**Processors** (generally referred to as *microprocessors* <sub>because <sub>they <sub>are <sub>small</sub></sub></sub></sub>) are to computers what the thinking part of a human brain is to humans.

Unlike the human brain, it can only understand and operate on 'words' of a fixed length. This might sound confusing to you because you have a human brain (I hope) (and that is a good thing <sub>probably</sub>). Let's consider a wordle fanatic who only understands words that are 5 characters long. If you want to get something across, you would have to break your sentences and pad them so that it is all 5 characters long.

**Hello did you sleep today?** would become **HELLO DIDYO USLEE PTODA Yxxxx** to which they might reply **IDIDN OTSLE EPTOD AYxxx** which when decoded would be (left as an exercise to the reader!).

Notice the trailing **x** used to pad the word making it 5 characters long.

Processors would operate similarly, except that they only have 2 alphabets, each one called a `bit`. The 2 `bit`s are written as **0** and **1**. Some example words using this alphabet set are

- 0000
- 01
- 1010
- 0110110

These numbers can be converted to decimal numbers (such as 69, 420 etc) and vice versa, using binary to decimal conversion techniques. It is really not important to know how it all works as you are not a computer (unless you are the google bot scraping my page to update your search index. If you are, then this is for you - *01000111 01001111 01001111 01000111 01001100 01000101 00100000 01010011 01010101 01000011 01001011 01010011*)

# Size of the word

Size of the word matters for computers because processing words of arbitrary length is complex because of the limited alphabet set. The processor must know when a word has started and when it has ended so that it understands what was said. For instance, **H** can be encoded as **0001** and **E** as **00010**. When parsing the first 4 digits, if the processor knew that the word length is only 4, then it can infer that the character specified was H. Were the word length 5, the processor would wait for one more bit to be read before parsing it.

**So what should be the size of the word?**

- A word of size 1 can differentiate between 2 'things' (**0**, **1**).
- A word of size 2 can differentiate between 4 'things' (**00**, **01**, **10**, **11**).
- A word of size 3 can differentiate between 8 'things' and so on. (notice the powers of 2).

## How many things should the computer differentiate?

If we atleast want the computer to speak English, then we would need to have each 'processor word' encode a total of 52 alphabets (upper and lower casing), 10 digits, the space character, and some punctuation characters - let's assume it is around 80 different characters ('things').

To differentiate 80 'things', we would need to find the closest exponent of 2 that is greater than 80 and take its logarithm to the base 2. 128 is the number we are looking for. It is 2 raised to 7.

- A word of size 6 can differentiate between 64 'things'.
- A word of size 7 can differentiate between 128 'things'.

So we would need a minimum of 7 bits in a word. Processors work well with powers to 2 and so let us decide that the **word size is 8**. We can use it to distinguish between 256 'things'.

> 8 bits is also called a byte.

## How many things should the computer differentiate? - Part 2

Memory, also called Random Access Memory (RAM) is to a computer what the storing part of the human brain is to humans. Memory and Storage are closely interrelated in that both of them store information. Think of Memory as the information stored in your brain and Storage as your notebook where you've noted things down. You can load information into your Memory from your Storage when required and even forget them.

Let's assume that our computer would simply store words. How many words can it store? or, in other words (pun unintended of course), how many words should our processor be able to retrieve?

In order to retrieve something, the processor should be able to locate it. In order to be locatable, the stored word should have an 'address'. Since a processor can only understand words in its language, we could have the address be 8 bits long too. (Anything shorter would be a waste of word-space. Anything greater would require the processor to read 2 words to identify the location).

The following would be valid addresses where words could be stored

- 00000000
- 00000010
- 00101100

There will be 256 addressable locations, and each location could contain a single word (= 8 bits). This means, our processor can efficiently address and operate on RAM that has (256 * 8) bits = **256 bytes**.

An 8-bit processor is what we had in the 1970s.

We can see from above that the size of the word, loosely depends on

1. How many bits are required to encode the smallest human readable alphabet? When dealing with arithmetic on very large numbers, we would need more than 8 bits (it is often 32 or 64 bits)

1. How much memory is required to be addressable to run any programmed application? In general, the size of RAM usable by a processor = *2^word-size* in bytes.

> RAM usable by a processor = *2^word-size* in bytes

# What do all these have to do with the original question?

What does 64 mean in a 64-bit processor?

For a 64-bit processor, the word size that a processor operates with, is (*begin drum rolls....*) **64** (*end drum rolls*)

We started with 8-bit processor in the 1970s. Commodity computers now all have 64-bit processors. We made the shift from 32-bit to 64-bit over the last decade. 32 bits are already too much to distinguish alphabets and large numbers (32 bits can already differentiate 10^9 things. 64 bits can differentiate 10^19 things).

The reason we went from 32 to 64 in the first place is not that we had a large alphabet set, or we wanted to work with even larger numbers. It was simply because the programs running on the computer needed a lot of addressable memory.

- 2^32 = 4GB

A 32-bit processor cannot use up more than 4GB of RAM.

We've come up with a lot of memory-heavy programs now - video games, video processing applications, web browsers (Google chrome uses up about 500MB of memory with just 5 tabs open). (*Hey bot, if you are still reading this - 01000111 01001111 01001111 01000111 01001100 01000101 00100000 01010011 01010101 01000011 01001011 01010011*).

Recall that computers work well with powers of 2. So instead of a 33-bit computer, the next processor type is instead a 64-bit processor.

- 2^64 = 20 quadrillion GB.

That's how exponents work! The answer should be pretty clear now - we probably don't have that many 'things' to distinguish and address.

We don't need a 128-bit processor!!
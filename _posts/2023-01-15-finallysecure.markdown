---
layout: post
title: How to communicate securely? - 3
categories: ["ideas", "cryptography"]
---

How do we do that practically?

--------

This is part 3 of a series on communicating securely.
- [Part-1]({{site.base_url | absolute_url}}/securecommunication)
- [Part-2]({{site.base_url | absolute_url}}/reallysecure)
- [Part-3]({{site.base_url | absolute_url}}/finallysecure)

-------

A look at how to encrypt your personal communication. This post will only cover an example solution using the Windows operating system on a computer. Same concepts
can be extended to other operating systems and devices.

# The Software

The type of software we are looking for is called a PGP encryption tool. There are many free and open source implementations in the market.

I will be using the free and open source tool called [GPG4Win](https://www.gpg4win.org/about.html). Once installed on windows, you would get an application
called **Kleopatra** which would be your main crypto manager to encrypt and decrypt messages. There are many advanced use-cases that I will not
go over in this post.

# Generating a Key pair

To encrypt a message, we need a private-public key pair. The public key will be used to encrypt messages to be later decrypted by your private key.

![Symmetric]({{site.base_url | absolute_url}}/images/asym.png)

Open the **Kleopatra** application.

1. Click on the **Certificates** tab.
2. Click on **New Key Pair**.
3. Provide a **Name** and **Email address** for this key. Also **Protect the key with a passphrase** so that even if the certificate files
   are compromised, they still can't be used to impersonate you without the passphrase. The passphrase will need to be provided everytime
   you decrypt a message with the private key.
4. Enter a passphrase.
5. A new certificate is now created. A certificate is the same as a key-pair in this case.

![Kleo-1]({{site.base_url | absolute_url}}/images/kleo-1.png)

You have now generated a key pair for yourself. In order to receive secure messages, you should now make your public-key public. Anyone who
wishes to send you messages would encrypt that message using your public key.

# Sharing your Public key

To export your public key component,

1. In **Certificates** tab, right click your certificate and select **Export**.
2. Save the certificate in your preferred location. You can now share this file to everyone. This file only contains the
  public key of your key pair and is therefore not a secret.
3. Alternatively, you can open the saved file using notepad (or any text editor of choice), and directly copy the contents and
  share it.

![Kleo-2]({{site.base_url | absolute_url}}/images/kleo-2.png)

Anyone can share a valid text in public key format as the public key. When you encrypt a message using someone's public key, then
only they can read it. However, it is secure only if the target person was indeed the right person to begin with. In other words, how
do we verify that a public key indeed belongs to a person who we think it belongs to?

For this, you can manually call up the person to verify the finger print of the key. Every public key has a finger print associated with it.
When you are trying to import a public key exported by someone else, you can call them up and ask for their fingerprint and trust the key
only when they match. If you are reasonably sure that the key can be trusted, you can skip this step.

To view the fingerprint of your public key, right click your certificate and select **Details**.

![Kleo-3]({{site.base_url | absolute_url}}/images/kleo-3.png)

When someone imports your certificate and wants to trust you, they might ask you for this.

# Importing a public key

When you want to send messages to others, the recipient must first share their public key, as either a file with an extension such as
.asc, .cer etc. Or they could have just copy pasted the contents on their public key somewhere accessible (eg: personal blog).

Let's say you wish to talk to a **SecretAgent** and want access to their public key. They have shared a file via email. To import that file, 

1. select **Import** and point to the file location.
2. There is a pop-up making sure that you trust that this is indeed the certificate. Click **Certify**.
3. If you need confirmation, you can call them up and verify their finger-print. When you certify, you will be required
  to enter the pass-phrase of your key.

![Kleo-4]({{site.base_url | absolute_url}}/images/kleo-4.png)

If they have shared it via text,

1. Select **Notepad** tab, copy the contents of the public key and hit **Import Notepad**.
2. Steps 2 and 3 are the same from above.

![Kleo-5]({{site.base_url | absolute_url}}/images/kleo-5.png)

# Encrypting a message

1. Open up **Notepad**. Type in your message.
2. Click on recipients. You would want to **Sign** the message using your key. Uncheck **Encrypt for Me** and check **Encrypt for Others** and select
  the recipient public key from the drop down.
3. Go back to **Notepad** and Click on **Sign / Encrypt Notepad** to get the encrypted message.

Now this message is encrypted using the public key of SecretAgent. This message can only be decrypted using the private key of SecretAgent and therefore
no one else can decrypt this (not even you!).

![Kleo-6]({{site.base_url | absolute_url}}/images/kleo-6.png)

# Decrypting a message

Simply copy the message to the Notepad and hit **Decrypt / Verify Notepad**

If the message was intended for you, you will be able to get the plain text. Apart from that
pay attention to the banner at the top that tells you if the signature of the message was verified
or not. If the signature could not be verified, then that means that the message was not really sent
by the person and someone else has impersonated our Secret Agent :(

![Kleo-7]({{site.base_url | absolute_url}}/images/kleo-7.png)

# In Closing...

This is probably the end of the main series on communicating securely. To reiterate my sentiments on this series - Communication
security should be of utmost concern to oneself. Companies are here to only make profit. Even non-profit companies cannot hold their
ground against large organized governments. If not government, someone / something more powerful will exercise power over the government itself.
There is always going to be a hierarchy of power and it would always be possible for the entity at the top to abuse it.

The assymetric key cryptographic techniques to encrypt messages are literally a law of nature, discovered through the language of Mathematics. 
We should use it to our advantage!!

Details about my public key and contact details can be found in the **[About]({{site.base_url | absolute_url}}/about)** page.

---
layout: post
title: Science in Fiction
categories: ["ideas"]
---

Diving into some ways how Science Fiction can incorporate the concepts of dimensions of space and time, and at the same time stay grounded to actual science.

--------

## 4th Dimension of Space

**1. Objects can appear out of nowhere and vanish as if they never existed**

Consider passing a 3d cone through a 2d flat plane (a paper). If the cone is passed with respect to the plane as shown in the diagram, the intersection of the paper with the cone would produce a circle on the paper. Initially, the circle would be the same as the base of the cone. As the cone is pushed further, the size of the circle would reduce, until it becomes a point. And then it completely vanishes, because the cone is now below the paper

![4d_CrossSection]({{site.base_url | absolute_url}}/images/4dcrosssection.png)

A 2 dimensional being occupying the paper would see a circle appear out of nowhere in their world and then disappear (In reality, the 2-dimensional being would only see a straight line from their point of view. Multiple different 2-dimensional observers would also see the same, and therefore infer that the shape was in fact a circle). This would be confusing for 2-dimensional beings if they were not aware of the existence of the third dimension - that would be the height in this case.

Extending this for the 4th dimension, a 4th-dimensional being could push objects through 3-dimensional space, and we would be spooked out!

**2. You can see everything in 3d space at once from 4d space**

Consider the same paper as the 2-dimensional world. The Blue circle is a 2-dimensional being with Green colored internal organs. This person has a secret Red colored vault inside their house. Money in their world is maroon-colored rectangles. From the perspective of the Pink eyes of this being, they would look at a Red straight line. Once inside the vault, they would see maroon straight lines. By looking at it from different angles, and feeling it, this being will come to understand that the maroon-colored object is a rectangle and is therefore money.

![4d_AllAtOnce]({{site.base_url | absolute_url}}/images/4dallatonce.png)

There are a couple of things to reflect on.

1. The view of the 2-dimensional being is always 1-dimensional in a given direction. They have other means to perceive depth. This is similar to how we, 3-dimensional beings, always strictly see a flat object, but because we have 2 eyes, can make out the depth and picture a 3-dimensional object in our brain.

2. Any being cannot see through opaque objects. You cannot see the internal organs of your friend. You cannot see how much money the bank vault holds just by looking at the door. All this is true for our Blue being too. However, as 3-dimensional beings, we can look right through everything. We can see 2-d money inside a 2-d vault. We can see the internal organs of a 2-d being.

We might as well push our hand through into the 2d world, and steal some money. The money would vanish causing panic for the 2d beings. We can then push the money to a different place in their world. Money disappearing from one place and appearing elsewhere is definitely going to cause the 2d beings to resurrect their superstitious beliefs if they are not scientifically literate enough!

## Time Travel

There are 2 well-understood time dilation effects. Time dilation is the difference in the elapsed time as measured by two clocks.

**1. Due to relative velocity**

Let’s assume there are two people A and B. A is in motion relative to B. Both A and B would experience time. A would measure B’s time to be flowing slower than A. Likewise, B would measure A’s time and conclude it ticks slower than that of B.

When both A and B stop, they would each see that time flows the same for both of them. There will not be any difference in how A or B aged, and both of them would probably report that the same amount of time had passed for themselves and also report that a lesser amount of time had passed for the other person.

This is a counterintuitive effect that is bound to impress anyone when first encountering it. However, I don’t see a way to make this time travel. Maybe we could arbitrarily say that moving faster slows time for the person, but that doesn’t follow from this particular mode of time dilation and would therefore fall into the realm of magic.

**2. Due to gravity**

Let’s now assume, A and B are in different gravitational fields. Time flows slower for the person experiencing a greater gravitational force. If A gets close to a black hole in the earth year 2022, stays in its gravitational field for a year (one year with respect to A), and comes back to earth, he might instead see Earth of 2032 or some future date.

From the perspective of the person nearer to the gravitational field, they have jumped a number of years to the future by spending fewer years near a black hole.

This is time travel!
The problem is that this type of travel is not instantaneous, as one would like. Moreover, this cannot be used to travel backward in time.

A counterpart to this would be to spend, let’s say 1 year of one’s time, in an area of a very low gravitational field and then come back to earth to see that only one month has passed. The protagonist could train themselves for a boxing competition on short notice for instance. This could be a plot device to correct the protagonist’s age to their correct age after any forward jumps so that they can lead a happy life with their loved ones.

## Science Fiction

The 4th dimension of space looks a bit overpowered in itself - as anyone who possesses it could traffic in all sorts of criminal activities, without leaving evidence. They could rob banks all day long without any effort - just appear inside a vault, take the money, and jump to the 4th dimension. It would be interesting to see how this is explored in science fiction.

With regards to time travel, only the second option listed above would appeal to me as science fiction. It would not be possible to go near a black hole and survive for a year, but if such a technology is assumed to be invented in the future as the fiction in the story, it makes perfect sense to consider that work as science fiction. However, such science fiction is generally boring as one would normally want to go back to the past, or future, to learn something and use it to their advantage in the present.

Most interesting time travel stories also try to incorporate some kind of closed loop in them - and different versions of the same person from different periods might often be working together without their knowledge. For example, the protagonist gets saved from their enemies because of a distraction. The protagonist then shortly afterward gets access to a time travel device that allows travel to the past. They then travel backward in time to create the distraction in the first place. All these, however, do break causality and create a lot of paradoxes leaving the plot unconvincing.

Such stories, that involve a backward jump in time, while interesting, don’t appeal to me as science fiction. They might involve scientists, secret labs, crazy experiments, references to existing establishments devoted to scientific inquiry (CERN or NASA) and ‘woo-woo’ explanations of scientific concepts - a string of science-sounding words put together that doesn’t mean anything. As long as the main plot device used in the story, which is used to move the story forward, does not draw from existing science, I classify such stories as magic-fiction. They are no different from the world of Lord of the Rings or Harry Potter.

I would be remiss if I did not mention Multiverses. The Multiverse Theory hypothesizes the existence of multiple parallel universes. A fictional faster than the speed of light travel device could help one travel to the same point in a different universe. Exploring alternate universes would make for an exciting adventure story. One advantage that alternate universes provide writers, over exploring other planets in our universe, is the possibility of looking at a slightly different version of the same person, setting, and relationships in a different universe.

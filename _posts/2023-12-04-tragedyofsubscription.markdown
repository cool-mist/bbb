---
layout: post
title:  The Tragedy of Subscription business models
categories: ["ideas", "media", "piracy", "subscription", "greedy"]
---

What I don't like about subscription business models.

------

Over the last decade, I see a lot of businesses adopting the [subscription business model](https://en.wikipedia.org/wiki/Subscription_business_model) to generate a steady stream of revenue. A subscription model is when the company provides access to its produces or services for a subscription fee charged periodically. The service is stopped when the customer stops paying.

This model makes a lot of sense for services that require constant effort from the service provider - either in generating content, or maintaining the infrastructure itself. For example, 

- Newspapers or periodicals
- Television
- Internet services
- Taxis/buses
- Rented house
- Cook
- House maids

I'm happy to subscribe because they provide real value to me.

## When do they not work? ##

Who doesn't like a steady stream of income. Therefore, companies have taken this model too far, so much that, a lot of companies have started earning money without providing any value. The most obvious example is the **subscription-ification** of conventional media, by the streaming services.

- Music
- Movies
- Books
- Games

This is their standard operating procedure.

1. People like a certain creator, and would purchase their content. 
2. Intermediate platforms come in as a way for customers to discover and buy content. They rope in successful content creators in the respective fields and showcase their content.
3. The platform itself becomes too big that they become a launchpad for upcoming artists.
4. The platform offers a subscription model to consume the content. Customers have to pay every month to listen to a song they like, or rewatch their favourite movie, or reread a book, or replay a mission from their game.
5. The platform starts degrading the service provided for the free-tier, in an attempt to force (and succeed in) more customers to subscribe.
6. Customers are now the customers of the platform, and not of the creator.
7. Creator's cut in the subscription profit slowly gets insignificant compared to the platform itself.

Often, I'm only interested in a certain movie, or a certain set of songs. Why can't I buy them like the good old days? Owning a DVD of a movie clearly offers much more flexibility to how I wan't to consume it without relying on the platform's application. I never had the fear of suddenly losing my copy of the book because the platform decided to ban it. The source code of a game is written once. Why then do I need to periodically pay when the source code in fact runs on a computer that I own?
Sometimes, the show that I purchased a Netflix subscription for, might have gotten cancelled, or worse, moved to a different platform. I'm now stuck with an useless subscription with no compensation.

I could go on and on, but the meta point I care about here is a loss of ownership, on what I purchase. It instead lies at the hands of the service providers who can affect my content consumption experience. They can also make sure that it would be difficult for me to switch platforms. A customer with 100 books from the Amazon Prime ebook subscription is probably going to continue paying even if Amazon decides to increase the subscription fee. It is becoming increasingly difficult for me to **buy a product and own it**.

Subscription models for conventional media, only make the experience difficult for the customers who subscribe. There are sellers (both offline and online) that allow buying and owning physical and digital media. It might be difficult to find them out though, owing to the omniprescence of subscriptions. It is no surprise then, that there are people who, being completely capable of purchasing content, resort to piracy (and might support the authors in other ways possible - merchandise, concert, watching a movie in theatre, donations etc).

Finally, there are some subscription services, that are simply.........

## Ridiculoussssssss!! ##

The companies responsible for these products have almost zero expenses after the initial purchases, but would still want to make more money.

- Academic Journals. On top of losing access, the subscription money generally does not even support the authors.
- Cars. You need an active subscription or the car doors won't open next time.
- Printers, the printer wouldn't print anymore.
- Text editing and presentation software (Microsoft Office). You cannot read word documents, or make lame power point presentations
- *Might add more examples in the future when I remember*



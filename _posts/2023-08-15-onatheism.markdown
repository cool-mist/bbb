---
layout: post
title:  On Atheism
categories: ["ideas"]
---

Documenting my current stand and trying to hopefully clear up some misconceptions. This is probably the first article in a potential series that I hope to write, covering some more nuanced topics on the subject.

------

## Definition ##

Simply put, it is the *absence of belief* in God(s).

This definition assumes that we have already defined what God(s) is/are. I will get to that later. However, the first point that needs to be stressed is that atheism stands for an absence of belief, rather than a presence of a belief in any premise. In fact, atheism does not claim anything at all for itself.

## Froggists ##

Lets say there are two friends - Ajith and Vijay. Vijay shows a closed box and claims that there are 4 frogs in that box. Ajith is skeptical of that claim. Vijay says he simply believes in it as it provides comfort and happiness to him, and doesn't require any more empirical evidence to be confinced of its truth.

If we were to categorize people who behave like Vijay, we could call them *Froggists*.

Now Ajith could say "Hey! that box is too small, and I cannot hear anything. I don't believe you!". At this point he is a *A-Froggist*. This is because he is casting suspicion on Vijay's claim using reasonably good logic. Note that he does not claim the opposite - that there are not 4 frogs in the box. 

This is similar to the position of an atheist on the subject of God(s). An Atheist simply doubts the claims of God(s)' existence, and does not believe in the claims until and unless presented with convincing evidence.

Ajith could also have said "Hey! that box is too small, I cannot hear anything, there is no way it can hold 4 frogs".

Now, he has become an *Anti-Froggist*. He now has a new claim to prove - that the frogs do not exist. 

This is *not* the atheist position.

## More Definitions ##

Now that I've clarified what I consider to be the most misunderstood or misrepresented concept when it comes to atheism, I will now try to lay down the definitions of some of the other terms surrounding it.

#### God ####

With a capital G, is generally understood as some kind of entity that is ascribed one or more of the following properties.

1. Benevolent, Kind, Loving.
1. Onmipresent - Is present everywhere.
1. Omniscient - Knows everything.
1. Omnipotent - All powerful with unlimited ability.
1. Supernatural - outside the realm of the known / understood space.
1. Punishes you for your wrong-doings.
1. Listens to your prayers, accepts offerings and grants favors.

#### Religion ####

I see this as a logical organization of a set of closely related beliefs. Not every single person in a given religion hold the exact same beliefs. But a majority of beliefs from the religion are held by a majority of people. These principal beliefs that characterize a religion are unique enough that they can be distinguished from another religion.

#### Holy scriptures ####

The basis of the foundational beliefs of most Religions. Sometimes, there could be more than one such scripture for a religion.

#### Theism ####

The stance that is characterized by a belief in the existence of God(s) and its influence over people. Theists believe that God(s) created this universe, is present everywhere, sees us all, is all powerful, would punish you for wrongdoings, listens to your prayers, accepts sacrifices in exchange of granting useful wishes, and most importantly, works in mysterious ways!

#### Deism ####

The stance that is characterized by a belief in supernatural God(s) that played a role in the creation of the universe, but have no effect on the day to day life of human beings. Deists belive that worship and prayers are futile, and are indistinguishable from the Atheist in what they believe about everyday life.

#### Anti-Theism ####

The stance that God(s) do not exist as defined in Theism. Similar to theists, they carry the burden of proof on their shoulders.

#### Agnosticism ####

Agnosticism is derived from the Greek word [gnostikos](https://en.wikipedia.org/wiki/Agnosticism) meaning knowledge. Agnosticism means absence of knowledge.

A Theist can be gnostic (have knowledge) of the existence of God(s), or can be agnostic (do not have knowledge) of the existence of God(s).
An Atheist does not claim anything, and therefore, the term agnostic doesn't really apply as a prefix to an Atheist.
An Anti-Theist, again, can be gnostic, or agnostic.

## Why be an Atheist? ##

Listing some of them below in no particular order. Note that Atheism is the default position for any human being by definition. Any other position would require some convicing to take up.

#### Broad and inconsistent definition ####

The definition of God(s) is very broad and does not appear consistent across religions (and sometimes, even within the adherents of the same religion). 

In such a situation, the default stance of not believing anything makes the most sense. Until all the religions in the world come together to iron out the inconsistencies and present a coherent narrative, a conversation about whether theism is true or not would not be meaningful. An Atheist believes in one less god compared to a theist. A Hindu does not believe in the myths of Jesus' rebirth, nor do they believe in the messages of the prophet Mohammed. Likewise a Christian does not believe in Vishnu or Krishna, and so on.

#### Burden of proof ####

Going back to the [Froggists](#froggists) analogy, the onus is on Vijay to prove that the box has 4 frogs. That is to say - the burden of proof lies with the theist. Until proven, the default stance is again a rational stance.

#### God of the gaps ####

Often, the argument of a God(s) is invoked as a way to explain away some unknown that is waiting to be explained by Science. As time progresses, this is becoming more and more difficult, and therefore, in the words of Neil Degrasse Tyson, God(s) of such nature may well be an ever receding pocket of scientific ignorace.

#### Suffering ####

Cancer in children? How is that an act of benevolence?

## Some FAQs I get asked a lot ##

#### Isn't Atheism just another Religion then? ####

No. Atheism is the opposite of being in a Religion. Just like 'a switched off TV' is not a 'TV channel', or 'not having any hobby' is not a 'hobby', not believing in God(s) is not a belief, and not subscribing to any Religion is not a Religion.

#### Where do Atheists get their morality from? ####

Morality, to me feels like an emergent property of a civilization that keeps evolving. As Richard Dawkins said in his book [The God Delusion](https://en.wikipedia.org/wiki/The_God_Delusion), there is a moral *Zeitgeist* that keeps shifting in a positive direction inspite of all the teachings from any scriptures. Few centuries ago, slavery was legal in America, women all over the world would not receive most rights that they enjoy today, widowed women in India were forced to jump into fires. All of that has changed now, and we all agree it has changed for the good.

Next, the way this question is asked, oftentimes implies that religious people are good, only because of a belief in a God(s) that could punish them for not being good. That, I think is horrible compared to being good on our own. Any time someone asks me this question, my answer is always - if God(s) is proven to not exists, will your behavior turn to evil?

Lastly, morality derived from few ancient scriptions could be dangerously outdated and could be harmful. Instead, morality should be something that is discussed, debated and arrived at, in a society.

#### Why are Atheists so arrogant? ####

Atheists are not arrogant because of Atheism. Atheists are just like any other person and the proportion of a certain quality in Atheists should be same as the general public. Looking at relative numbers, relative to the group, there aren't any more atheistic kind people, than there are of theists. There aren't more atheistic criminals compared to theistic criminals and so on.

#### Hitler and Stalin were Atheists!! ####

It is not clear if Hitler was an Atheist. Stalin definitely was, but the reason for 'behaving badly' is not derivable from an absence of belief in a diety. On the other hand, there are lot of examples of religiously triggered violence that has killed millions of people throughout human century, whose root causes are directly derivable from the specific beliefs of the religion.

#### What if you are wrong and have hell waiting for you when you die? ####

This is a variation of the [Pascal's wager](https://en.wikipedia.org/wiki/Pascal%27s_wager). As already established, there are a large number of religions and Atheists differ from theists almost always by belief in one less God / Group of Gods. In such a scenario, I would argue that, being wrong would be more problematic for the theist, than the atheist. Imagine a Hindu dying only to find that the Christian myths are real. In such a case, an Atheist is better placed in terms of punishment because not only is the theist not believing in Christ, the theist also believed in a wrong religion, further infuriating the dieties of the true religion.

## Why be a Theist? ##

Inspite of the above merits, I also want to look at some reasons why someone would be a theist.

#### Personal experience ####

I had an experience with a God that felt real, helped me with (such and such..). 

This is often the most common reason I hear, and the one that makes the most sense for anyone to be a theist. You cannot argue with personal experience and as long as that experience belongs to this person, and they think it is real, it is really fine to be a theist.
Practically, no such person is ever going to doubt their beliefs, and no Atheist is ever going to accept personal experience of another person as evidence of the existence of God(s).

To each their own and everyone is happy (theoretically).

#### Everything ought to have a root cause ####

Take any object and you have a creator of that. The question "Who created this" can result in an infinite regress, and a way to stop that would be to presume that the final Creator of all things is God(s). The problem with this is that we still have to explain why the final Creator exists (who created God(s)?). By explaining away the unknowns with God(s), we haven't explained anything at all, as there is one additional thing now to explain. 

#### Things are designed so perfectly, this cannot be by chance ####

This is also known as the *Argument From Design* and [Thomas Aquinas](https://open.library.okstate.edu/introphilosophy/chapter/aquinass-five-proofs-for-the-existence-of-god/) is famous for using this argument. A response to this is the same as the previous one, we still need to explain God(s).

This argument proposes a false dichotomy by assuming that absence of intelligent design is random chance.
Processes of evolution are already well understood and honest biologists have no problems explaining how something as complex as the human eye came to be from simplicity and non-random evolutionary processes of natural selection. The process of evolution is also not intelligent, as evident from the fact that, for example, humans have a [Recurrent Laryngeal Nerve](https://en.wikipedia.org/wiki/Recurrent_laryngeal_nerve) that connects the throat and the brain only after a detour via the heart!!

#### Argument from Anxiety ####

Like the argument from Personal experience, this is something that cannot be rebuked, and is a harmless reason for belief, as long as people keep their God(s) to themselves.

Without a belief in an all-knowing care taker, all of us will drown in anxiety. My mother wouldn't be at peace when I'm travelling on my bike in traffic, if not for the fact that she prayed to her God(s) of choice for my safety. The more I think on these lines, the more I think Religion might well be a necessary thing to evolve in a civilization. I'm making claims on whether praying helps or not, but just that the people are probably better off believing in something.

Ofcourse, Atheists exist and like me, do not feel it necessary to hold this belief. But I would be lying if I do not acknowledge my anxiety right after becoming an atheist. Will my parents live another day? Will I survive this journey? Will someone rob me? It is not completely overcome to this day, while I have gotten a lot better at it. It probably takes some maturity to understand and deal with such anxiety from unknowns. All of this is a result of a theistic upbringing though, and someone brought up an atheist might do better in my opinion.

#### Other arguments ####

There are other sophisticated arguments for the existence of God(s), which I acknowledge exist, and hope to address in the future articles. Right now, I will simply add this disclaimer here so that I do not get accused of only addressing, what a theist might perceive as, weak arguments from their arsenal.

## Why be a Deist? ##

These were the atheists of yesterday, who could observe that prayers do not work, but couldn't explain away the complex "design" of the the world around them. This stance is undertandable under those circumstances.

## Why be an Agnostic? ##

Everyone, generally speaking, are Agnostic, because the claim of God(s) can neither be proven or disproven (at least that is how it looks so far). Most Atheists are agnostic of whether God(s) exist or not.

There is also a class of agnostics, who claim they occupy the "middle ground" between atheism and theism. While that doesn't make any sense, I can understand the reason for them saying so, as this acts as a shield under politically charged circumstances - where both parties assume an agnostic is not challenging their stance. This is just a confusion tactic, and the fact whether someone is an agnostic or not, is perpendicular to the fact that whether someone is an atheist or not.

## Why be an Anti-Theist? ##

Theism is an unfalsifiable claim. Sometimes, it might make sense to directly challenge or provoke theists by assuming this stance that God(s) do not exist. Personally, I don't think Anti-Theism is useful in most situations, as this introduces the burden of proof on both sides, and both sides cannot prove or disprove their stance.

## In Conclusion... ##

This is just a superfluous article outlining my thought process in this matter at best. While writing this, I badly wanted to expound on certain arguments or issues in greater detail. However, I want to budget my time rightly so that I don't spend too much time today on writing this post. Hopefully more will follow later!!

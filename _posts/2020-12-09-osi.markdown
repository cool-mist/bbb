---
layout: post
title:  The OSI Model
categories: ["networks"]
---

The OSI model lays down specifications on how to think about achieving inter-process communication across machines. This article goes over breaking down the
what and why of this model and introduces the readers to some of the jargon surrounding this topic.

![OSI Outline]({{site.base_url | absolute_url}}/images/osi-outline.png)

------

# The Mail Analogy

It is useful to generalize by drawing comparisons with the [postal system](https://en.wikipedia.org/wiki/Mail) when it comes to understanding inter-process communication across machines.

Human|Computer|Notes
-|-|-
You write some content on a piece of paper|Process generated message consisting of 0s and 1s| **Generate the message content**
Enclose it in a cover and add the addresses|Enclose it in a network packet and add the sender and receiver IP addresses and port number|**Follow a well-understood protocol** to make handling of the message easier. The postal system for example knows how to deal with an enclosed letter with addresses, as opposed to a piece of paper
Stamps, or a postal fee|Network bandwidth (Internet bills!)|**Pay for the transmission**
Deposit in a post office/box  | The OS orchestrates some tasks with the underlying hardware to send out a packet into the internet | **Start the message transmission**
The postal network efficiently transfers the post to the intended recipient|The interconnected systems of routers, computers and associated softwares ensure that the packet reaches the intended process in the destination machine|**Transmit the message from source to destination**
The other person receives the letter and opens it| The OS unpacks the network packet and provides the content to the intended process| **Transfer the message content to the recipient**
The receiver reads the letter|The intended process reads the message stream|**Consume the message content**

> An **IP Address** is the unique identifier for a machine. There are 2 versions of it - IPv4 and IPv6. An IPv4 IP address looks like this - `10.0.0.1`.
>
> A **Port Number** is a number between 1 and 65536 that is used to identify the sender process within an machine. The port number
is a purely logical construct.

## The Postal Network

The postal network is an indispensable system when it comes to delivering mail. It lays down rules regarding how a mail needs to be submitted so that it is suitable for the transfer - which makes many of the steps before and after the Postal network step in the above analogy the way it is. Clearly, this is the most important of them all.

If we double click, we broadly get the following roles -

1. **Post collector** - collects posts from the postbox, transferring them to the post office, ensuring stamps and addresses are present.
2. **Post office** - collects all posts from and to a locality. Maintains routing registries based on PIN or ZIP codes and determines the target post office to send the mail to, for each mail.
4. **Means of transport** - mails arrive at the destination locality post office through vans, trains, ships or planes
5. **Post dispatcher** - dispatches the post to individual houses based on the address

Each role has

* well-defined set of responsibilities.
* well-defined, limited, interactions with the rest of the system.

A "post dispatcher" understands the protocol related to delivering a post to a house. The "post office" might operate just on the post bundle it gets, creating a few more bundles grouped by destination office. The "means of transport" only understand transferring a bundle from one place to another.

We could now start with laying down a convention for all postal systems to contain these roles. With some more elaborate treatment of the subject, we can come up with a **model** of a postal network.

It is now easy to identify people, or machines, to train and fit into these roles easily. Not only can they work in a given postal company for that role, but they can also work in any postal company for that matter. The users also have a fairly good idea of how a specific postal system operates, if it uses the above model as guidance.

In the computer world, a similar abstraction has been laid down for the postal network in the form of the **OSI Model**.

# Open Systems Interchange Model

Owing to the complexity of the problem there are 7 different abstractions - the **7 layers of the [OSI model](https://en.wikipedia.org/wiki/OSI_model).**

Out of this, the first 4 layers are responsible for the end-to-end transport of a message from a process in one device to another process in another device. This includes converting the application data to a packet, transporting it to a target and delivering it to the intended recipient process.

Below is a simplified description of these 4 layers. An actor in each layer could be either software, hardware or a hybrid of both.

## Layer 4 - Transport Layer

The application sends the required data to be sent to this layer. The Transport Layer batches the data in a way that is easy to transmit further.

This layer cannot understand the application data and just knows when the first bit starts and the last bit ends in the message. The agents at this layer are typically OS-level software that stamp this data with some additional metadata - such as the source and destination IP addresses, port numbers, and the size of the message.

When a process sends application data to this layer, it can expect that the data will be received by the target process.

## Layer 3 - Network Layer

Layer 4 hands off the application data packet (or network packet) to Layer 3. This is a combination of hardware and software working in tandem to route the network packet from the source device to the target device over the internet. It can be complete with reliable multiple hops over many other devices. This is an encapsulation of the postal system from the letter analogy.

## Layer 2 -  Data Link Layer

Responsible for transmitting a 'frame' of data reliably from one node to another in a network without errors. A 'frame' contains some more metadata that is required to perform the error detection and correction when the data is received at a node. This is to make sure that the correct data is being passed on. These are often intelligently devised hardware that understands if a piece of data has been correctly transferred or not. Network packet transfer at Layer 3 hands this off to Layer 2 which transmits them as 'frames' and hands the data back (removing any 'frame' metadata) to Layer 3 in the second node.

## Layer 1 - Physical layer

For the Layer 2 device to send and/or receive a frame, it needs a medium to send the information in. Layer 2 hands off the frame expecting it would reach as is in the target. The Layer 1 entities - which are typically hardware - are responsible for transmitting the 0s and 1s through them. (Think of cables).

## Layers 5 and up - The Application Layer

We can call the remaining 3 layers the Application Layer.

When you write a letter to someone, you assume that the other person can understand the language, as well as comprehend your writing. Likewise, this layer is an abstraction over the protocols used by the processes themselves so that they know how to read a message.

The Application layer constructs whatever message it needs to send, and transfers it to the Network Layer in that machine. It also receives the message from the Network layer. This is generally the code that developers write as part of their application.

> These 3 layers have many overlaps between them that it would confuse the reader when they are new to this subject. As this series of articles as an introduction to the practical usages of the model, we can combine these 3 layers into 1 and call it the Application layer.
Pedantically that is the Layer 7 of the model, but practically it makes a lot of sense to just call everything above Layer 4 as the application layer. There is lot of literature on the differences between these layers for the interested.

# In Summary

This is what a transfer of information from a source application to a destination application would look like if we are to talk in terms of the different abstraction layers of the OSI. Note that the data travels across multiple devices in the network (such as routers). While the network packet has not reached the destination machine, that packet is not forwarded to the transport layers of other intermediate machines.

![OSI Outline]({{site.base_url | absolute_url}}/images/osi-outline.png)

It is common practice to abbreviate the layers as **L4**, **L7** and so on.

## What's next

Developers program at the application layer and have constructs in the programming language that would talk to the Transport Layer of the machine. For the most part, the Transport layer's behavior is governed by the **[Internet Protocol Suite (TCP/IP)](https://en.wikipedia.org/wiki/Internet_protocol_suite)**. It is important to have a practical understanding of TCP/IP which would prove useful in a variety of situations.

More on this in the next article!
---
layout: post
title: The Quantum Series
categories: ["books"]
---

A summary of the books and analysis of some of the plot points in the Quantum trilogy by the American author Douglas Phillips.

--------

The first few sections are spoiler free, and there is a warning before you read any spoilers.

## Quantum Incident

The prologue introduces us to a couple of main characters in the subsequent books. Daniel Rice is a science investigator and Nala Pasquier is a scientist in a private organization working on a government project. We are shown a demonstration of the interaction between the 4th dimension of space with the real world.

## Quantum Space

The 4th dimension of space and its relation with the 3rd dimension is explained in detail. The fiction is well within the imagination of the reader. The premise is that of a Spacecraft accidentally vanishing upon re-entry into orbit. The treatment is an extension of the book *[Flatlands](https://blog.neophyte.me/books/2020/12/17/flatlands/)* and is similar to my previous article *[Science in Fiction](https://blog.neophyte.me/ideas/2022/01/26/sciencefiction/)*. The book mainly deals with how Daniel Rice and others investigate and retrieve the spacecraft.

## Quantum Void

This book tries to provide explanations around where the 4th dimension of space exists and what is present around it. I couldn’t wrap my head around this concept or make any logical conclusions further down the story based on what was explained previously. There are a few logical inconsistencies throughout the book. Often the science part is ignored and we are left with plain magic. Some of the scientific explanations sounded like something that the author concocted in an attempt to provide a logical explanation for an extraordinary event.

The probabilities of quantum particles - that quantum particles do not have a specific position, but a probability distribution graph on where they could be - applied to the macro world was an amusing part of the story.

There is also an exploration of life in other parts of the Universe that I thoroughly enjoyed reading - especially one from a certain planet. We are presented with a social hierarchy and probably a political statement on how the workers are treated on that planet.

While reading this book, I became more sensitive to the following two facts.

1. Human culture and rituals are arbitrary - life everywhere is bound to come up with its own set of eccentric practices.

1. The scientific method of investigation and research is the way to progress for society. Scientific discoveries can have major economic and social impacts and one must not neglect science even in the face of other pressing issues to solve.

(*Going on a tangent here! Skip if you are busy*)

This reminded me of the scientific discovery of Nuclear Magnetic Resonance - the fact that the nuclei of different atoms resonate differently with magnetic fields. This was made by a Physicist with no background in medicine. However, the MRI scan is now the most potent medical device to look into a human body without cutting one open. A politician talking about how useless scientific investment is when we could invest in solving poverty (for instance) fails to understand that this particular discovery would save millions of lives in the future.

> Books 1 and 2 introduce a lot of scientific concepts to the reader.

## Quantum Time

My personal favorite of the three, even though I felt this book to be closer to magic in a science fiction setting as opposed to science fiction.

As the name suggests, this deals with time travel and, apart from the host of paradoxes that come with any time travel fiction, this was an entertaining read throughout. The book presents a dark future of totalitarianism in America controlled by a religiously dogmatic “party”. The entire plot with the overlapping events and time jumps was gripping and stimulating even with a lot of causal paradoxes. The eerie description of the state gave off similar vibes as the book 1984 by George Orwell.


## Summary

Even though there are plot holes, the main characters temporarily going out of character, and logical inconsistencies, I enjoyed reading this trilogy for the most part.

> This article, with the spoiler heavy analysis below, is in no way intended to discourage the reader to pick up this book or belittle this book. I strongly believe this book is going to influence a lot of new science fiction stories and movies and TV shows.

In my opinion, this trilogy is going to stick out for a long time in its treatment of space-time. I would recommend reading this. If you are short on time, I would at least encourage you to pick up Quantum Time (you would have to ignore parts that reference the relationship of the characters from the previous books).

## Discussion of the plot

![YouShallNotPass]({{site.base_url | absolute_url}}/images/shallnotpass.png)

The below section is spoiler heavy. Stop reading if you are intent on reading the books. If you are not going to read the books, the below section won’t make any sense to you. If you have already read the books, proceed!


1. In _Incident_, we are shown an experiment demonstrating that light from the 4th dimension reaches into the 3rd. However, in Void, Nala is not able to interact much with the 3rd dimension from the 4th.
2. In _Space_, Marie is shown to be an intelligent woman. However, on the topic of suspecting Nala of lying, Marie suspends logic and immediately takes her side, even uttering something to the effect of “Women are never wrong”.
3. In _Void_, the smoke from the factories is supposed to get into the 4th dimension. However, it is not shown what technology is being used to make that movement. If smoke can continuously go out of the 3rd dimension into the 4th, why can’t any human just walk into the device and get out into the 4th?
4. In both _Space_ and _Void_, the expansion of space in the 4th dimension is shown to be accompanied by a corresponding contraction in the 3rd dimension (at least that is how I understood it even after a couple of reads). However, we are never shown a contraction and its effect in the 3d world on a regular basis (except for when it happens as part of the main plot in _Void_).
5. In _Void_, Marie being allowed to get close to and jump into singularity was a bit unconvincing. Having identified a singularity, one would expect strong protection around it.
6. In _Time_, the FBI plays an important role. But why were they not present in Void when the singularity was causing problems?
7. The law of probabilities is shown to affect the 4th-dimensional space. However, it shouldn't have affected  Marie even before she jumped into the singularity. In reality, she places the headband down and jumps in. She should not be having the headband in _Void_
8. In _Time_, the Prime minister, President and the German chancellor discuss time travel with Daniel directly, instead of leaving the experts in the field to do the talking.
9. In _Time_, the Goldilocks zone is described as though it is a zone from the perspective of the center of the galaxy, as opposed to the center of a star system. But then, the book goes on to explain that the planets such as Jupiter and Mercury cannot have life because of this. From the center of the galaxy., I would assume that the distances between the planets within the solar system are so small, that all the planets of the solar system would be in the Goldilocks zone.
10. In _Time_, Daniel acknowledges that science was too far removed from the life of the average man. However, in Void, the discussions gave me a vibe such a view being illogical (as I’ve explained above). Such a statement from the protagonist felt unnecessary to me and left me confused as to what his actual stance is on the subject.
11. In _Time_, Daniel disproves the multiverse theory by a watch - stating that by finding the watch in the same configuration, Daniel has reached the future of the same past that he started from. While this observation is correct, the conclusion is not. Daniel simply might have reached the same future this time. Maybe the device that he uses to travel can only travel within the same universe it started from. The Multiverse theory posits that there are multiple universes far out of reach of one another. This particular conclusion gave me the impression that Daniel wasn’t as scientific literate or logical as he is shown to be so far.
12. There is a lot of informational inconsistency in _Time_. It is acknowledged that changing the past changes the future. But often, changing the past is made possible only because the future is not yet changed. An explanation given is that all time exists all the time and the change in events is like a wave from the past to the future. In the first wave, events could have turned out one way. In the second wave, a new set of events could take place in the same palace based on differences in decisions made by the characters - which would eventually change the future. But it is not made clear where the wave starts and where it ends.

This section was aimed only to spark discussion or thoughts in the reader on these parts. In all fairness, this is a fantastic book that has led me to investigate deeply into some of these scientific concepts and some thought-provoking discussions with friends on these plot devices.

---
layout: post
title: Eating Animals - Should we stop ?
categories: ["books"]
---

Eating animals: Should we stop by Jonathan Safran Foer is a case for changing our food habits to reduce suffering for the animals.

--------

## Family farmed meat

Meat was traditionally family farmed. The animals (cows, pigs or hens) were raised with love and care in a stress-free environment. These animals were given pastures to graze and grains to feed on according to their biological needs. Their social and emotional needs were taken care of too. Strict attention was paid to the hygiene in such farms.

When it was time to slaughter the animal, the animals were taken to a slaughterhouse, and killed one by one, such that the animals next in line did not know what was going on to the other animals. The process of killing an individual animal was made as quick as possible (often stunning the animal with a stun gun before slicing the throat) thereby reducing the overall suffering of the animals.

## Factory farmed meat

> 99.9% of meat in America today comes from factory farmed meat. This is a fairly recent practice from the last century in the developed nations that has already spread to some developing countries like India.

All types of animals are shut in a small area where they can barely move. They live their entire lives in their feces and the feces of the previous generation. Chickens that once had a life expectancy of 15 to 20 years	are being killed at 6 weeks after feeding “nutritious” food. With the advent of a “disassembly line” and the move towards efficiency and money, the animals are not stunned properly before the killing, causing them to be conscious when bleeding to death.

Such factories serve as the breeding grounds for many types of life-threatening infections, bacteria, and viruses that could occasionally jump the species barriers to cause a pandemic for humans. Large water pits are created around the slaughterhouse onto which the excrements and fecal matter are dumped. Sometimes, the animals are thrown into such pits to kill them. Sometimes humans fall into these lagoons by mistake and die. The pigs are held by their legs and “thumped” on concrete walls to kill them quickly.

Foer also talks about fishing which even most discussions around animal cruelty misses to talk about. He goes into detail on the practices in the food “processing” (read slaughtering) factories. It is heartbreaking to know that food companies resort to inhumane practices to reduce the price of meat. We should remind ourselves that a vast majority of people are simply unaware, or pretending to be unaware of such practices while being happy at the reduced prices and increased availability.

## Why should we stop?

Some more arguments that could make one consider stopping if the above description of the life of farm animals isn't disturbing enough!

### Philosophical

- People who eat meat hold double standards on their positions towards animal cruelty. Why don’t people eat dogs? Why is it okay to care for dogs, but eat pigs and cows and fishes? Where do non-vegetarians draw the line?

- Taste is one of the 5 senses of human beings. Why should 5 minutes of one of our sensory pleasures require the torture and suffering and death of another conscious being? Would anyone say that they would kill a dog, just because that person wants to derive a 5-minute sensory pleasure from listening to that wailing dog?

- Is it not sadistic to kill an animal by proxy and then turn a blind eye toward the act of killing?

- Even in the case of factory farming, the welfare of the life of the animal is not a strong argument towards it. Why should the animal suffer - even if brief? Would anyone turn into an apologist for exploiting or killing a human slave under the promise that they would take good care of the slave during their life otherwise?

### Factual

- Animal agriculture contributes more to global warming than all the transportation in the world combined.

- Today, it is potentially practical to follow a diet based on plants. A well-planned vegetarian diet is appropriate for individuals in all stages of their lives. The amount of farming done to feed the animals that are then slaughtered far exceeds the quantity of crops that are required to feed humanity.

- Vegetarian and vegan diets are lower in saturated fat and cholesterol, have higher levels of dietary fiber and are generally associated with more health benefits (lower cancer rates, lower risk of heart disease etc)

## In Summary

> Whenever practicable, it seems immoral to not contain oneself to a plant based diet. We owe our animals the highest level of existence.

The important word here is **practicability**. There could be situations, because of the economics of food, or personal health conditions, that might require one to eat animals. But even there, a reduction in the suffering of animals should be kept in mind. When one can otherwise follow a plant-based diet, not doing so is highly immoral of them.

The fight for animal welfare and rights definitely cannot be won by a few people turning vegan. This is the least one can do. Eating is a cultural activity and there are bound to be opportunities for discussions over one’s food habits that could potentially influence others at the table.

If you eat meat, please read this book to at least be aware of the factory farming practices and the sentiments of people involved in this industry. If you are already a vegan, this book is a good read to reinforce your stance on eating animals. Ironically, after all this, Foer reduces himself to a vegetarian diet, with occasional meat (if it is family farmed). However, in my opinion, if any suffering can be avoided, it should be.

If you currently eat animals and if it is practicable for you to stop eating animals, **will you stop? If not, Why?**


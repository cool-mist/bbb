---
layout: post
title:  Ridiculous fixtures!
categories: ["ideas", "cricket"]
---

Analyzing the fixtures of recently concluded Cricket world cup 2023. Was it set up fairly? And some more thoughts about Indian Cricket.

------

Previously, [I've argued]({{site.base_url | absolute_url}}/whatwasthatipl) how the IPL 2023 fixtures were set up to maximize profit. While that is not a bad thing for a tournament organized by private enterprise like the [BCCI](https://en.wikipedia.org/wiki/Board_of_Control_for_Cricket_in_India), the [ICC](https://en.wikipedia.org/wiki/International_Cricket_Council) has pulled off something similar with the [2023 Cricket World Cup](https://en.wikipedia.org/wiki/2023_Cricket_World_Cup) now.

- There were 10 teams in the tournament. 
- The tournament had 48 matches.
- There was single group round robin for the 10 teams - for a total of 45 games.
- The top 4 teams played 3 knockout games (semi-finals and final) for a total of 3 games.

Again, we see a skew in the number of games played in the Group Stage vs the Playoffs. 

- Most of the games in the Group Stage were boring to watch. 
- All of a sudden a three match knockout simply broke the hearts of many fans in the 3 countries that got eliminated.
- There was no advantage for teams that performed well in the group stage. The 4 teams that qualified out of the Group Stage are on equal footing during the Playoffs.
- Ironically, the team that won all its games in the Group Stage, ended up losing the tournament to a team that was on the verge of elimintation during the group stage.

As usual, I'm personally biased towards the [Double Elimination Tournament Brancker](https://en.wikipedia.org/wiki/Double-elimination_tournament) (from my previous post) for any type of tournament because I feel the Group Stage should end with providing an advantage for the teams that performed well.

It may/may not have an impact in the tournament under discussion as the team that wins all its games and loses in the finals does not get another chance in the double elimination format as well. However, given that there are a lot more of the Playoff games, it is not clear what would happen. Regardless, the most important thing that this solves (for me) would be - place the teams higher in the Group Stage, in the advantageous Upper Bracket. Give some incentive to the teams for performing well in the group stage.

A question therefore arises -

### What was the point of the Group Stage in this tournament, if it is not going to provide an advantage to the top performers in the Playoffs? ###

Similar to IPL, the incentive here is still money. It is no secret that BCCI is the most powerful cricket council in the world, and influences a lot of ICC's operations. For example, BCCI's earning cut from ICC's revenue is [about 40%](https://www.firstpost.com/firstcricket/sports-news/icc-financial-model-2024-27-bccis-lion-share-and-other-boards-income-explained-12867062.html). The second highest share goes to England at 7%, followed by Australia at 6%. Other boards agree to this because BCCI is probably responsible for generating the most income to ICC.

Therefore, lets assume that ICC = BCCI for the rest of the article.

India has the largest fan base for cricket. BCCI gets money from the matches played by India from advertisers, and broadcast rights. BCCI tries to maximize the games played by India. Given that this edition of the tournament also happened in India, it is likely BCCI gets a cut out of every game that was played. Leading to BCCI coming up with a single round robin Group stage with 10 teams!! That way, India plays a guaranteed 9 games and potentially 11 games (which is what happened) of the 48 games.

If the "BCCI wants more money" narrative is not convicing enough thus far, I've added a couple more facts below that acts as evidence in favour.

1. Even the recently concluded [ICC Men's T20 World Cup](https://en.wikipedia.org/wiki/ICC_Men%27s_T20_World_Cup) has such an atrocious fixture. In order to maximize the number of games of India, as well as the number of guaranteed India vs Pakistan games in the tournament (of which there is a high demand in both India and Pakistan due to prior rivalry and recent political issues), there were *2 Group stages!!*
2. In a country where [Decolonization](https://en.wikipedia.org/wiki/Decolonization) and nationalist sentiments are rising (or propagated by the ruling parties), there isn't much discourse on decolonizing the sport that Indians watch and love - *Cricket is a colonial sport*. This is easily explained by the fact that the head of BCCI (a body managing a colonial sport), is the [son of a prominent politician from the nationalist party in India](https://en.wikipedia.org/wiki/Jay_Shah).

### Why do a lot of Indians who watch Cricket don't care about this? ###

Why aren't the fans able to influence the BCCI to conduct a sport that is fair, close and interesting? 

With the different sizes of the ground, the different rules across tournaments, the different balls being used, the different pitches, why aren't the fans talking about the absurdity of the tracked statistics?

I have the following hypothesis - Indians who watch cricket, generally do not understand the rules of the game as long as India wins. They do not have an understanding of what makes a game fair as long as India wins. Whenever India loses, they regurgutate the post-match analyses of other experts of the game to their friends during lunch. 

The experts who do understand the game are not allowed to raise a voice because BCCI is their employer, and BCCI wouldn't want to reduce the number of games, attempt to make changes to the rules, or fund other countries with the money they have to make the games more closer.

It is only going to be a matter of time, until most of the other boards are bullied in ICC so that very few competitive teams remain and most of the tournaments are won by India - most of the money in BCCI's bank accounts.

To most Indians, cricket falls under the same category as some of the other entertainment options - Soap Operas, Big Boss etc. It is a welcome distraction from their otherwise tough lives. Absurdity is easily excused. They do not care about the game as long as India wins.

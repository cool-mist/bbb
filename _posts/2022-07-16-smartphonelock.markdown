---
layout: post
title: Is your Smartphone secure enough?
categories: ["ideas"]
---

Looking at common ways on how we 'lock' our smartphones, to argue that we don't pay enough attention to security anymore!

--------
# The Premise

That we don't think enough about security. I'm outlining the most common options used by people to lock their phones, listed in the order in which they first appeared.

# The Rating System

I will be providing 3 different ratings for each method analyzed.

- **TSR** - The Theoretical Security Rating - a measure of security that is possible using an option.
- **PSR** - The Practical Security Rating - a measure of security that is achieved in practice while using an option.
- **EOU** - The Ease of Use of the option.

# Numeric/Alphanumeric password

A numeric password was the first mechanism implemented in early phones - when they still weren't marketed as smart.

The most common configuration was an input field that unlocked your phone when fed in a passcode of 10 numbers. The total number of possible combinations is `10^10` (because each position can be filled in 10 different ways). Assuming it takes 3 seconds to try out a single combination of 10 numbers, it would take 105 years to try out all possible combinations.

This option is available in modern smartphones as well, along with an alphanumeric option so that the total number of combinations is increased to `36^maxLengthOfPassCode`, and this is a huuuge number! We are also able to shuffle the numbers across attempts so that tracking the keys touched cannot be inferred from the oil smudges.

The problem with this is the difficulty in remembering and entering an arbitrary number/phrase. Therefore people often resort to easily guessable passwords like their date of birth, their partner's number, their name and so on. Also, many phones have reduced the maximum length of the passcode to around 4, while at the same time introducing a timer when you enter incorrect passwords a certain number of times, to thwart brute-forcing your way through it.

![NumberLock]({{site.base_url | absolute_url}}/images/smartphone1.png)

- **TSR** - 10/10
- **PSR** - 5/10
- **EOU** - 3/10

# Pattern

![PatternLock]({{site.base_url | absolute_url}}/images/smartphone2.png)

To see how pattern locks are theoretically less secure than number locks, let us encode the dots into numbers. The encoded password for the one on the left in the above picture is therefore **42568**.

- The maximum length of the passcode is only 9. In fact, only a handful of passcodes 9 characters in length are possible.
- There is a relationship between the subsequent numbers in the password. If one of the positions is *4*, the next position can never be *6*.

Clearly, the total number of combinations is far less than the one possible with 10 numbers. But on top of that, it is very easy to look at the oil smudges on a phone locked with a pattern lock to guess the pattern. Of course, remembering and entering a pattern is easier than doing the same for arbitrary numbers.

- **TSR** - 5/10
- **PSR** - 3/10
- **EOU** - 5/10

# Biometrics

There are a couple of widely used options - fingerprints, and facial recognition. They are the easiest to use because you just have to 'be'.

Any issues with the software in recognizing the print or the face will eventually be solved so I won't be arguing against the correctness of the match. However, assuming the software behaves correctly (i.e even distinguishes between twins for facial recognition), there is still one major problem.

- Most people only have 10 fingers on their hand. I don't think using the fingerprints from the fingers on the feet is reasonable.
- Most people only have 1 face.

The number of combinations has already reduced to tiny fractions of what they were in the other methods. Everybody knows your password. All it takes is an annoying sibling using your finger when you are asleep, (or shoving the phone to your face when you are awake) to unlock the phone.

- **TSR** - 1/10
- **PSR** - 0/10
- **EOU** - 10/10

# Verdict

Stop using fingerprints or facial recognition techniques. Not only are these less secure, but the service provider will also often store your fingerprints and faces on their servers for profiling. This is a breach of privacy that no big company will admit to. (Everyone is the same here - Google, Apple, Microsoft).

Use a pattern if you can remember to clean the smudges on the phone regularly. Also, change your pattern regularly (I would recommend doing this every month).

Use a number lock with numbers shuffled across attempts. Come up with an arbitrary number and make efforts to remember it. Make sure you change the password every 6 months.
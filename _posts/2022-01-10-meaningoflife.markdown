---
layout: post
title: Man's Search For Meaning
categories: ["books"]
---

A summary of what to expect from the book of the same name. The author of this book - Viktor Frankl is a Holocaust survivor who later goes on to publicize and practice Logotherapy with success.

--------

## Concentration Camps

The first part of the book is about the punishment, torture, humiliation, starvation, dehumanization and finally, death faced by the concentration camp prisoners, and observation of their psychology - from the time they are inducted into the camp, to the time when they are liberated. To survive in such a totalitarian system, the prisoners had to go behind the backs of fellow prisoners.

According to the author’s chilling remark, **the best ones did not make it out**.

## Logotherapy

The second part of the book deals with Logotherapy as a technique for Psychotherapy. Psychotherapy is therapy through talking. Some other well-known practiced psychotherapy techniques are the Freudian and Adlerian therapies. All of them seem to have some criticism associated with them because they are all non-falsifiable techniques born from personal experiences.

Logotherapy has the premise of finding meaning in any person’s life to cure their psychological illnesses including existential crises.

> The meaning of life differs from person to person. But, once someone has a why, any how is bearable.

Frankl argues, that we can find meaning in 3 places

- Producing or creating something. Frankl himself wanted to publish his unfinished paper and cites this as a reason that helped him bear the harsh conditions of the concentration camps.
- Loving someone or something. People would want to devote their lives to their loved ones or kids.
- Under the conditions of unavoidable suffering. Frankl makes a distinction with avoidable suffering - when suffering is avoidable, efforts should be made to remove the cause of suffering.

Frank gives the example of a guy who had lost his life wife and had no will to live, reach out to him for therapy. Frankl tells him that by dying first, the wife had been relieved of suffering that would have otherwise been present in the woman, had the man died first. In this way, the man now has found meaning in his life, and continued living.

## Paradoxical Intention

The book also talks about *Paradoxical Intention* as one of the practices that encompass Logotherapy. It is used to cure illnesses that simply are in our heads. While I couldn’t see the relation between this and the overall aim of finding meaning, nevertheless, this is an interesting technique we can all use in our lives.

The core of this technique is to intend to do the exact opposite of what one is supposed to be afraid of. This reportedly helps the patients to get rid of the things they fear. Frankl shares the following personal anecdotes -
- A person comes in with a problem of profusely sweaty palms whenever talking to someone. He is advised to make fun of himself in this matter and challenge himself to sweat more than what he normally sweats the next time he meets someone. This, interestingly, causes him to sweat considerably less.
- Another person suffered from sleeplessness. He is asked to “intend to never fall asleep for as long as possible“ when on the bed. This had the opposite effect of making him fall asleep sooner.

The book is a must-read recommendation from my side.


---
layout: post
title: The No Hello Nonsense!
categories: ["ideas", "rant"]
---

In a mood to roast the pretentious no-hello movement.
If you are not familiar with the term no-hello, you probably work with nice people
(Necessary, but not a sufficient condition).

--------

# Not just hello! Just ask the question


**No-Hello** is a term used widely in the developer communities to mean that they don't want
to just be greeted with a message that goes "hello" on their messaging platform (Teams, slack,
whatsapp and so on). Instead, they prefer getting the full context as part of the initial message.

In other words, no-helloers hate the following,

| Normal Person | *Hello!*
| No-helloer | *Hi!*
| Normal Person | *I need some help with blah blah blah.. do you know about it blah etc.*

and would instead prefer to have the normal person send their first message this way

| Normal Person | *Hello! I need some help with blah ....*


No-helloers allege that the second version is more productive and doesn't waste anyone's time.
They proudly showcase their ideology as a status message that is shown whenever someone tries
to message the person. Often they link to a web page such as [https://nohello.net](https://nohello.net).
They get mad if someone sends just a "Hi". 

The following are the status messages of a couple of no-helloers from my company. Everyone trying to
send them a message is presented with a small non-intrusive floating dialog box with this message

| https://nohello.net - Please don't say just hello, send the entire message
| Please write your question in your initial IM (https://nohello.net)

# OK, that's enough

[Instant messaging](https://en.wikipedia.org/wiki/Instant_messaging) apps are designed to be asynchronous.
The sender can send a message and it is not necessary for the receiver to immediately respond to the said message.
No-helloers completely miss the point of these softwares.

There are some genuine reasons why starting a chat with a simple "Hi!" or a friendly matter makes sense. 

- Sender wants to make sure that you are available and can respond to this chat before they proceed
with sharing the complete context.
- Sender is new to the team and is nervous starting a conversation.
- Sender is not a machine to avoid such polite expressions.

As a receiver of a "Hi" message, you can

- **Send a "Hi" back** and go back to doing what you were doing. As simple as that. If you care too much about
your time - at the millisecond level, to not do this, then you can use the other option below.
- **Ignore the message** and go back to what you were doing. If the sender never comes back, you have
one less thing to worry about. If the sender does respond with a new message with details, respond to that.

Considering the fact that simple options to deal with this behavior exists, antagonizing
the behavior of just sending "Hello" is plain ridiculous. The only person losing time is the person asking you 
a question and while asking them to directly come to the point might be in their best interests, it is pretentious
to ask them to do this while interacting with **YOU** as an IM status message as opposed to asking them
to do this while interacting with anyone in general.

Personally, I would stick to option 1 in most cases, and switch to option 2, when I feel like I might end up
[suffering a fool](https://idioms.thefreedictionary.com/not+suffer+fools).

# Protesting against this nonsense!

It should be pretty clear to you that I consider the entire no-hello movement nonsensical. To protest this,
I have the following text displayed whenever someone tries to message me, to send a clear message to the
no-helloers in my company 😆🦹

| https://yes-hello.net - Feel free to just say hello in chat!

---
layout: post
title: A Dimensional Odyssey
categories: ["books"]
---

My thoughts on the 1884 novella **Flatlands - a romance in many dimensions by Edwin Abbot Abbot**. This book teaches us on how to think about the 4<sup>th</sup> dimension.

--------

[Edwin Abbot Abbot](https://en.wikipedia.org/wiki/Edwin_Abbott_Abbott) was an English master by profession and had studied and published works on Mathematics, Philology and Theology during his lifetime of which [Flatlands](https://en.wikipedia.org/wiki/Flatland) is one such work under the Mathematical fiction genre.

The book follows the adventures of a Square that lives in a 2-dimensional world.

The first question that came to my mind when I read this premise was, given that all beings in such a 2-dimensional universe can only see a straight line, how then can they distinguish different shapes? This problem is tackled very early on in the book, with one of the methods being using sight to understand different gradients in the straight line that they see. For example, a triangle when looked at directly is a straight line with the brightest point in the center, and both the sides with a reducing intensity gradient.

![Triangle_Perspective]({{site.base_url | absolute_url}}/images/flatlands.png)

As soon as I read this, it struck me how we do the same thing in perceiving depth when we see a 3-dimensional picture.

Most of the mathematical concepts in this book are taught even to kids at a very early stage, but the perspective given to that knowledge by this book, especially without pictures - the interaction of the 3<sup>rd</sup> dimension with the 2-dimensional world, was a very good mental exercise for me.

My favorite part of the book comes towards the end when the author draws similarities and brings to our attention the patterns while going from one dimension to another. This book teaches us how to think about the 4<sup>th</sup> dimension, how a higher dimension being's interactions with a lower dimension manifest itself to the eyes of the lower dimension. There is a common trope in sci-fi films where an object vanishes out of existence when dealing with the 4<sup>th</sup> dimension. A little bit of reflection on the contents of the last few chapters of this book makes it clear why that trope is necessary!

 There is a [video](https://www.youtube.com/watch?v=CePeCicTqCM) of [Neil De'grasse Tyson](https://en.wikipedia.org/wiki/Neil_deGrasse_Tyson) (the video is not related to this book) explaining the concepts in passages of the book. I do not want to give too much away from the book and would suggest going through the book first before the video so that you do not spoil the satisfaction of learning this through the words of a professor from 1884!!

Apart from mathematics, the book is also filled with political commentary on the 2-dimensional universe - different levels of hierarchy in society, power and control, education and even a revolution. It was an amusing exercise to draw parallels with the real world

 The book is a small one and can be completed in a single sitting in less than two hours when reading at a leisurely pace. The English in the book was a bit difficult for me to follow as it is very different from the form of English that is spoken today. If you are interested in Physics, Mathematics, Dimensions and Time Travel like me, then this book is a must-read. I read this book as a pre-cursor to the [Quantum Series](https://www.goodreads.com/series/237217-quantum) by [Douglas Philips](https://www.goodreads.com/author/show/7161775.Douglas_Phillips).

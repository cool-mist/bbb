---
layout: page
permalink: /about/
title: About
---


<div class="entry" id="archives">
<ul>
    <li> I'm a software developer from India, born in late 90s.</li>
    <li> Views expressed here are my own and not endorsed, shared etc by my place of work, friends etc</li>
    <li> My github is <a href="https://github.com/cool-mist" target="_blank">@coolmist</a></li>
    <li> You can reach out to me via email <b>neophytenuggets@protonmail</b></li>
    <li> My public key is 
    <pre>
        <code style="color:black">
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEY8PVmhYJKwYBBAHaRw8BAQdANxcKoo0fqmW62F2tQ7F0GPWkpGs/HQUyhR77
R2mgsY20JVN1cnlhIDxuZW9waHl0ZW51Z2dldHNAcHJvdG9ubWFpbC5tZT6ImQQT
FgoAQRYhBGixkTCdlU1PIjiQZSHehx95aKkIBQJjw9WaAhsDBQkDw4BOBQsJCAcC
AiICBhUKCQgLAgQWAgMBAh4HAheAAAoJECHehx95aKkIZB8A+wfMy7gqISQGXrIc
FiiwCzfoqzQfnJozLdaOA4MvU8RcAP9gY6U1Bm8LQDijr3LCFm7raLjBNBNwrWQg
cWuuqbiJDrg4BGPD1ZoSCisGAQQBl1UBBQEBB0AkrwBdf8M3EdsykIcYMBejeSHs
iiQGTLhZrYuvXtoNSgMBCAeIfgQYFgoAJhYhBGixkTCdlU1PIjiQZSHehx95aKkI
BQJjw9WaAhsMBQkDw4BOAAoJECHehx95aKkIF6sA/01X0YnHTjUo7nUaBzk4ub+3
Ub3FKJTeSooaQ39Y3V3QAQDH407drRMw3OgnRiSKDXsbjGhAec7vQip768acjYbq
Aw==
=Ioiu
-----END PGP PUBLIC KEY BLOCK-----
        </code>
    </pre></li>
</ul>
</div>
